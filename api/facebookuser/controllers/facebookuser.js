'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
const { spawn, exec } = require('child_process');
const fs = require('fs');
const dgAuto = "/dg-auto"

const readFile = (filePath) => {
  if (fs.existsSync(filePath)) {
    return String(fs.readFileSync(filePath)).trim()
  }
  return ''
}

const checkTrackFile = async (filePath) => {
  const data = readFile(filePath)
  if (data) {
    return data
  }
  await timeoutPromise(1000)
  return await checkTrackFile(filePath)
}

const timeoutPromise = (time) => new Promise((res) => setTimeout(res, time))

const callCypress = (username, password) => {
  spawn(
    `export cypress_username=${username} && export cypress_password=${password} && npm run go-quite`,
    { cwd: dgAuto, shell: true }
  )
}

const deleteAnyway = (filePath) => {
  try {
    fs.unlinkSync(filePath)
  } catch (error) {
  }
}
module.exports = {

  async login(ctx) {
    try {
      strapi.log.debug(`Received request`)
      const username = ctx.request.body.username;
      const password = ctx.request.body.password;

      const trackFile = `${dgAuto}/data/${username}.track`
      const cookiesFile = `${dgAuto}/data/${username}.cookies`
      const jsonFile = `${dgAuto}/data/${username}.json`

      deleteAnyway(trackFile)
      deleteAnyway(cookiesFile)
      deleteAnyway(jsonFile)

      callCypress(username, password)

      strapi.log.debug(`Checking track file...`)
      const status = await checkTrackFile(trackFile)

      let fbuser
      strapi.log.debug(`Got status: ${status}`)
      if (status === 'DONE') {
        const cookie = readFile(cookiesFile)
        fbuser = { username, password, cookie, status }
      } else {
        fbuser = { username, password, status }
      }
      const entity = await strapi.services.facebookuser.create(fbuser)
      return sanitizeEntity(entity, { model: strapi.models.facebookuser })
    } catch (error) {
      strapi.log.debug(`Server error: ${error}`)
    }

  },

  async sendcode(ctx) {

    const { id } = ctx.params;
    const username = ctx.request.body.username;
    const code = ctx.request.body.code;

    fs.writeFileSynce(`${dgAuto}/data/${username}.json`, { code })

    let status
    let stop = false

    while (!stop) {
      status = await checkTrackFile(`${dgAuto}/data/${username}.track`)
      if (status === 'DONE') {
        stop = true
      }
    }

    const cookie = await readFile(`${dgAuto}/data/${username}.cookies`)
    fbuser = { username, cookie, code, status }
    const entity = await strapi.services.facebookuser.update({ id }, fbuser)
    return sanitizeEntity(entity, { model: strapi.models.facebookuser });
  },

};
