# CustomStrapi
## Contain custom APIs which will integrate to CustomStrapi
### Products
    Purpose: contain product info including name, type, oldPrice, newPrice, image and slot
    APIs: CRUD, find, and findone
### FacebookUsers
    Purpose: username, password, code, cookie, and status
    APIs: CRUD, find, findone, login and sendcode